const main = document.querySelector("main");

const emailSection = document.querySelector("#email-validation");
const errorBanner = emailSection.querySelector("#error");
const form = emailSection.querySelector("#email-form");
const emailInput = form.querySelector("input[type='text']");
const emailButton = form.querySelector("input[type='button']");

function setError(text) {
  if (text) {
    errorBanner.textContent = text;
    errorBanner.classList.add("visible");
  } else {
    errorBanner.textContent = "";
    errorBanner.classList.remove("visible");
  }
}

function clearError() {
  setError(null);
}

async function onValidateEmail(ev) {
  ev.preventDefault();
  const email = emailInput.value.trim();

  const response = await fetch("./auth", {
    method: "POST",
    mode: "same-origin",
    cache: "no-cache",
    headers: {
      "Content-Type": "text/plain;charset=utf-8",
    },
    body: email,
  });

  if (response.status === 200) {
    emailInput.disabled = true;
    emailButton.remove();
    emailInput.value = email;
    clearError();
  } else {
    setError(
      "LITERAL ERROR 403: This email is not allowed to book on our system. Please try again or contact us for help."
    );
  }
}

form.addEventListener("submit", onValidateEmail);
