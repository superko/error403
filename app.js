const express = require("express");

const app = express();
const port = 3000;

const authRoute = require("./routes/auth");

app.use(express.static("public"));

app.listen(port, () => {
  console.log(`Listening on port ${port}.`);
});

app.use("/auth", authRoute);
