require("dotenv").config();

const express = require("express");
const router = express.Router();
const validEmails = process.env.VALID_EMAILS.split(",");

router.post("/", express.text(), (req, res) => {
  const email = req.body;

  console.log(email);
  console.log(validEmails);

  if (validEmails.includes(email)) {
    req.app.locals.userEmail = email;
    res.status(200).send();
  } else {
    res.status(403).send();
  }
  res.end();
});

module.exports = router;
